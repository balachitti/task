import express, {Request,Response,NextFunction} from 'express';

import * as shop from '../controller/shopify.controller'

const router = express();

router.post('/ordervalidate',shop.ordervalidate);
router.get('/list',shop.listorder)

export {router as shopRoutes}



node_modules/
.env
.gitignore
