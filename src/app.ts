import  express  = require('express');
import {Request,Response,NextFunction} from 'express';
import dotenv = require('dotenv');
import cors from "cors";
import {shopRoutes} from './routes/routes'

import * as bodyParser from 'body-parser'

dotenv.config();
const port  = process.env.PORT || 5000;
class App{
    public app: express.Application;
    constructor(){
       this.app = express();
       this.config();
       this.allRoutes();
       
        }
    private config(): void{
       this.app.use(bodyParser.json());
       //this.app.use(bodyParser.urlencoded({ extended: true }));
       this.app.use(cors());
       const server = this.app.listen(port, ()=> {
        console.log(`Server is running on port: ${port}`);
     });
     }
     private allRoutes(): void{
       
        this.app.use("/api", shopRoutes);
      //  console.log('/api');
        this.app.get('/',(req:Request, res:Response) => {
           //console.log("entered")
           res.json(`backend is ready`)
         })
      }
    }
     export default new App().app;


