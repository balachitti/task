"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.listOrder = exports.ordervalidate = void 0;
var Shopify = require("shopify-api-node");
var dotenv = require("dotenv");
dotenv.config();
var option = {
    shopName: 'windsor-us.myshopify.com',
    accessToken: process.env.TOKEN || "null",
};
var shopify = new Shopify(option);
var ordervalidate = function (req, res) {
    var response = {};
    console.log(req.body.email);
    var name = req.body.order_number;
    shopify.order.list({ name: name }).then(function (result) {
        // result = JSON.parse(result)
        console.log(req.body.email, result.length);
        var order = result[0];
        console.log(order.billing_address.zip);
        if (order.email == req.body.email || order.billing_address.zip == req.body.zip) {
            response['status'] = "success";
            response['data'] = { "order_date": order.created_at };
            console.log(response);
            res.status(200).send(response);
        }
        else {
            response['status'] = "failure";
            res.status(404).send(response);
        }
    })
        .catch(function (err) {
        console.log(err);
        res.status(err.code).send(err);
    });
};
exports.ordervalidate = ordervalidate;
var listOrder = function (req, res) {
    shopify.order.list().then(function (result) {
        console.log(result);
        res.send(result);
    });
};
exports.listOrder = listOrder;
