"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var dotenv = require("dotenv");
var cors_1 = __importDefault(require("cors"));
var routes_1 = require("./routes/routes");
var bodyParser = __importStar(require("body-parser"));
dotenv.config();
var port = process.env.PORT || 5000;
var App = /** @class */ (function () {
    function App() {
        this.app = express();
        this.config();
        this.allRoutes();
    }
    App.prototype.config = function () {
        this.app.use(bodyParser.json());
        //this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(cors_1.default());
        var server = this.app.listen(port, function () {
            console.log("Server is running on port: " + port);
        });
    };
    App.prototype.allRoutes = function () {
        this.app.use("/api", routes_1.shopRoutes);
        //  console.log('/api');
        this.app.get('/', function (req, res) {
            //console.log("entered")
            res.json("backend is ready");
        });
    };
    return App;
}());
exports.default = new App().app;
